import 'package:expenseapp/models/transaction.dart';
import 'package:expenseapp/widgets/txcard.dart';
import 'package:expenseapp/widgets/txinput.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Home extends StatefulWidget {
  Home({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final List<Transaction> _tx = [
    Transaction(
        id: 't1', title: 'New Shoes', amount: 170000, date: DateTime.now()),
    Transaction(
        id: 't2',
        title: 'Urgently Payment',
        amount: 120000,
        date: DateTime.now()),
    Transaction(
        id: 't3',
        title: 'Weekly Groceries',
        amount: 250000,
        date: DateTime.now()),
    Transaction(id: 't4', title: 'Snack', amount: 30000, date: DateTime.now()),
    Transaction(
        id: 't5', title: 'Children Toy', amount: 70000, date: DateTime.now()),
  ];

  void _addTransaction(String titleVal, double amountVal) {
    final newTx = Transaction(
        id: DateTime.now().toString(),
        title: titleVal,
        amount: amountVal,
        date: DateTime.now());
    setState(() {
      _tx.add(newTx);
    });
  }

  void _startNewTranscation(
    BuildContext ctx,
  ) {
    showModalBottomSheet(
        elevation: 5,
        backgroundColor: Colors.blueAccent,
        context: ctx,
        builder: (_) {
          return GestureDetector(
            child: Txinput(_addTransaction),
            onTap: () {},
            behavior: HitTestBehavior.opaque,
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () => _startNewTranscation(context),
            color: Colors.white,
            iconSize: 35,
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Card(
                color: Theme.of(context).primaryColor,
                child: Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                  child: Text(
                    "CHART",
                    style: GoogleFonts.lexendTera(
                        textStyle: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold)),
                  ),
                ),
              ),
              Txcard(
                transactions: _tx,
              )
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: IconButton(
          icon: Icon(Icons.add),
          onPressed: () => _startNewTranscation(context),
          color: Colors.white,
          iconSize: 35,
        ),
      ),
    );
  }
}
