import 'package:expenseapp/models/transaction.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_money_formatter/flutter_money_formatter.dart';

@immutable
class Txcard extends StatelessWidget {
  final List<Transaction> transactions;
  Txcard({@required this.transactions});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(5, 2, 5, 2),
        height: 300,
        child: transactions.isEmpty ? 
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              child: Text("No Transaction List", style: TextStyle(
                fontSize: 18,
              ),),
            )
          ],
        ) 
        : ListView.builder(
          padding: EdgeInsets.symmetric(vertical: 15),
          itemBuilder: (ctx, i) {
            return Card(
              elevation: 5,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(right: 15),
                    padding: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        border: Border.all(
                          color: Theme.of(context).primaryColor,
                          width: 2,
                        )),
                    width: 150,
                    child: Text(
                      FlutterMoneyFormatter(
                              amount: transactions[i].amount,
                              settings: MoneyFormatterSettings(symbol: "Rp. "))
                          .output
                          .symbolOnLeft,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).primaryColor,
                          fontSize: 16),
                    ),
                  ),
                  Expanded(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(transactions[i].title,
                          style: TextStyle(
                              letterSpacing: 1,
                              fontSize: 16,
                              color: Colors.lightBlue)),
                      Text(DateFormat.yMMMMd().format(transactions[i].date),
                          style: TextStyle(color: Colors.grey[600])),
                    ],
                  ))
                ],
              ),
            );
          },
          itemCount: transactions.length,
        ));
  }
}
