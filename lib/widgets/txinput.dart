import 'package:flutter/material.dart';

class Txinput extends StatefulWidget {
  final Function addTranscation;
  Txinput(this.addTranscation);

  @override
  _TxinputState createState() => _TxinputState();
}

class _TxinputState extends State<Txinput> {
  TextEditingController titleController = TextEditingController();
  TextEditingController amountController = TextEditingController();

  void submitData() {
    final title = titleController.text;
    final amount = double.parse(amountController.text);
    if (title.isEmpty || amount <= 0) {
      return;
    }

    widget.addTranscation(title, amount);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      child: Card(
        elevation: 5,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            TextField(
              decoration: InputDecoration(
                  icon: Container(
                    //margin: EdgeInsets.only(left: 10),
                    child: IconButton(
                        color: Theme.of(context).primaryColor,
                        icon: Icon(Icons.book),
                        onPressed: () {}),
                  ),
                  labelText: "Title"),
              controller: this.titleController,
              onSubmitted: (_) => submitData,
            ),
            TextField(
              decoration: InputDecoration(
                  icon: Container(
                    child: IconButton(
                        color: Theme.of(context).primaryColor,
                        icon: Icon(Icons.attach_money),
                        onPressed: () {}),
                  ),
                  labelText: "Amount"),
              controller: this.amountController,
              keyboardType: TextInputType.number,
              onSubmitted: (_) => submitData,
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
              width: 200,
              child: RaisedButton(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      IconButton(
                          color: Colors.white,
                          icon: Icon(Icons.add),
                          onPressed: () {}),
                      Text(
                        "Add Transaction",
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  color: Theme.of(context).primaryColor,
                  onPressed: submitData),
            ),
          ],
        ),
      ),
    );
  }
}
