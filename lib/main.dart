import 'package:expenseapp/home.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Expense Personal',
      theme: ThemeData(
        textTheme: GoogleFonts.righteousTextTheme(),
        primarySwatch: Colors.deepPurple,
        accentColor: Colors.deepOrangeAccent,
        appBarTheme: AppBarTheme(
          textTheme: ThemeData.light().textTheme.copyWith(
            headline6: GoogleFonts.anton(
              color: Colors.white,
              fontSize: 24,
              letterSpacing: 1.5
            )
          )
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Home(title: 'Expense Personal'),
    );
  }
}
